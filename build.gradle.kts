plugins {
    java
    id("org.springframework.boot") version "3.0.4"
    id("io.spring.dependency-management") version "1.1.0"
    id("org.sonarqube") version "4.0.0.2929"
    jacoco
}

group = "com.caper"
version = "0.0.1-SNAPSHOT"

configurations {
    compileOnly {
        extendsFrom(configurations.annotationProcessor.get())
    }
}

repositories {
    mavenCentral()
}

extra["springCloudVersion"] = "2022.0.1"

dependencies {
    implementation("org.springframework.boot:spring-boot-starter-webflux")
    implementation("org.springframework.boot:spring-boot-starter-data-jpa")
    implementation("org.springframework.boot:spring-boot-starter-web")
    implementation("com.github.GBSEcom:first-data-gateway:1.15.0")
    implementation("com.cloudinary:cloudinary-taglib:1.33.0")
//    implementation("com.cloudinary:cloudinary-android:2.3.1")
    implementation("com.cloudinary:cloudinary-http45:1.33.0")
    compileOnly("org.projectlombok:lombok")
    developmentOnly("org.springframework.boot:spring-boot-devtools")
    annotationProcessor("org.springframework.boot:spring-boot-configuration-processor")
    annotationProcessor("org.projectlombok:lombok")
    testImplementation("org.springframework.boot:spring-boot-starter-test")
//    implementation ("org.springframework.boot:spring-boot-starter-security")
    runtimeOnly ("org.postgresql:postgresql")
}

dependencyManagement {
    imports {
        mavenBom("org.springframework.cloud:spring-cloud-dependencies:${property("springCloudVersion")}")
    }
}

tasks.withType<Test> {
    useJUnitPlatform()
}

tasks.test {
    useJUnitPlatform()
    finalizedBy(tasks.jacocoTestReport) // report is always generated after tests run
}

tasks.jacocoTestReport {
    classDirectories.setFrom(files(classDirectories.files.map {
        fileTree(it) { exclude("**/*Application**") }
    }))
    dependsOn(tasks.test) // tests are required to run before generating the report
    reports {
        xml.required.set(false)
        csv.required.set(false)
        html.outputLocation.set(layout.buildDirectory.dir("jacocoHtml"))
    }
}


// Exclude the incompatible module and use an alternative annotation processor
configurations {
    compileOnly {
        resolutionStrategy {
            eachDependency {
                if ((requested.group) == "org.springframework.boot" && (requested.name) == "spring-boot-configuration-processor") {
                    useTarget ("org.springframework.boot:spring-boot-starter:2.5.0")
                }
            }
        }
    }
}
sonarqube {
    properties {
        property("sonar.projectKey", "AdvProg_reguler-2023_mahasiswa_kelas-b_2106630025-Bonaventura-Galang-Kristabel-Angipanglipur-Hatmasasmita-_b8-caper_profile-service_AYeLKLRx9YbzwjBXjNK3")
        property("sonar.host.url", "https://sonarqube.cs.ui.ac.id")
        property("sonar.login", "11c7975eee81ec23074824b99b2b2a1f0e673bc9")
    }
}
