package com.caper.profileservice;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;

@SpringBootTest
class ProfileServiceApplicationTests {

    @Test
    void testApplicationStartsWithoutExceptions() {
        assertDoesNotThrow(() -> ProfileServiceApplication.main(new String[]{}));
    }


}
