package com.caper.profileservice.utils;
import com.caper.profileservice.dto.*;

public class TestUtils {
    public static UserDto createUserDokterDto() {
        return UserDto.builder().id("iduserdokter").username("usernamedokter").nama("namauserdokter").role(UserRoleDto.DOCTOR)
                .domisili("domisili").umur(20).password("password").build();
    }

    public static UserDto createUserPasienDto() {
        return UserDto.builder().id("iduserpasien").username("usernamepasien").nama("namauserpasien").role(UserRoleDto.PATIENT)
                .domisili("domisili").umur(20).password("password").build();
    }

    public static DokterDto createDokterDto() {
        return DokterDto.builder().id("iduserdokter").user(createUserDokterDto()).certificateUrl("url").build();
    }
}
