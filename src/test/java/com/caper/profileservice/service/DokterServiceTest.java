package com.caper.profileservice.service;
import com.caper.profileservice.dto.DokterDto;
import com.caper.profileservice.dto.UserDto;
import com.caper.profileservice.dto.UserRoleDto;
import com.caper.profileservice.repository.DokterRepository;
import com.caper.profileservice.repository.PasienRepository;

import com.caper.profileservice.service.dokter.DokterService;
import com.caper.profileservice.service.dokter.DokterServiceImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import reactor.core.publisher.Mono;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;


@ExtendWith(SpringExtension.class)
public class DokterServiceTest {
    @Mock
    private DokterRepository dokterRepository;

    @InjectMocks
    private DokterServiceImpl dokterServiceTest;

    @Test
    void testServiceFindAll() {
        // set up
        List<DokterDto> doctorList = new ArrayList();
        var dokter = createDokterDto();
        doctorList.add(dokter);
        var doctorListNew = Mono.just(doctorList);

        when(dokterRepository.findAll()).thenReturn(doctorListNew);

        // test
        var dokterResult = dokterServiceTest.findAll();

        assertEquals(doctorListNew, dokterResult);
    }

    @Test
    void testServiceFindByUsername() {
        // set up
        var username = "usernamedokter";
        var dokter = Mono.just(createDokterDto());

        when(dokterRepository.findByUsername(username)).thenReturn(dokter);

        // test
        var dokterResult = dokterServiceTest.findByUsername(username);

        assertEquals(dokter, dokterResult);
    }

    public static DokterDto createDokterDto() {
        return DokterDto.builder().id("iduserdokter")
                .user(createUserDokterDto())
                .certificateUrl("url")
                .build();
    }

    public static UserDto createUserDokterDto() {
        return UserDto.builder().id("iduserdokter")
                .username("usernamedokter")
                .nama("namauserdokter")
                .role(UserRoleDto.DOCTOR)
                .domisili("domisili")
                .umur(20)
                .password("password")
                .build();
    }



}
