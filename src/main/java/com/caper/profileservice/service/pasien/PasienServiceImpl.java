package com.caper.profileservice.service.pasien;

import com.caper.profileservice.dto.PasienDto;
import com.caper.profileservice.dto.UpdatePasienDto;
import com.caper.profileservice.exceptions.DokterDoesNotExistException;
import com.caper.profileservice.exceptions.PasienDoesNotExistException;
import com.caper.profileservice.repository.PasienRepository;
import com.cloudinary.Cloudinary;
import com.cloudinary.utils.ObjectUtils;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;
import reactor.core.publisher.Mono;

import java.io.File;
import java.io.IOException;

@Service
@RequiredArgsConstructor
public class PasienServiceImpl implements PasienService{
    private final PasienRepository pasienRepository;
    private final Cloudinary cloudinary;

    @Override
    public Mono<PasienDto> findByUsername(String username) {
        if (isPasienDoesNotExist(username)){
            throw new PasienDoesNotExistException(username);
        }
        return pasienRepository.findByUsername(username);
    }

    @Override
    public Mono<PasienDto> update(String username, UpdatePasienDto request) {
        if (isPasienDoesNotExist(username)){
            throw new DokterDoesNotExistException(username);
        }
        return pasienRepository.updatePasien(username, request);
    }

    @Transactional
    @Override
    public Mono<PasienDto> uploadTeethPicture(String username, MultipartFile certificate) throws IOException {
        var filename = username + "-certificate-" + certificate.getOriginalFilename();
        var tempFile = File.createTempFile("caper-", filename);
        certificate.transferTo(tempFile);

        var params = ObjectUtils.asMap(
                "public_id", "caper/" + username + "-certificate" + certificate.getOriginalFilename(),
                "overwrite", true,
                "resource_type", "image"
        );
        var uploadResult = cloudinary.uploader().upload(tempFile, params);
        var url = uploadResult.get("secure_url").toString();
        return pasienRepository.uploadTeethPicture(username, url);
    }

    private boolean isPasienDoesNotExist(String username) {
        return pasienRepository.findByUsername(username) == null;
    }
}
