package com.caper.profileservice.service.pasien;

import com.caper.profileservice.dto.PasienDto;
import com.caper.profileservice.dto.UpdatePasienDto;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import reactor.core.publisher.Mono;

import java.io.IOException;

@Service
public interface PasienService {
    Mono<PasienDto> findByUsername(String username);
    Mono<PasienDto> update(String username, UpdatePasienDto request);

    Mono<PasienDto> uploadTeethPicture(String username, MultipartFile certificate) throws IOException;
}
