package com.caper.profileservice.service.dokter;

import com.caper.profileservice.dto.DokterDto;
import com.caper.profileservice.dto.UpdateDokterDto;
import com.caper.profileservice.exceptions.DokterDoesNotExistException;
import com.caper.profileservice.repository.DokterRepository;
import com.cloudinary.Cloudinary;
import com.cloudinary.utils.ObjectUtils;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;
import reactor.core.publisher.Mono;

import java.io.File;
import java.io.IOException;
import java.util.List;

@Service
@RequiredArgsConstructor
public class DokterServiceImpl implements DokterService {
    private final DokterRepository dokterRepository;
    private final Cloudinary cloudinary;

    @Override
    public Mono<List<DokterDto>> findAll() {
        return dokterRepository.findAll();
    }

    @Override
    public Mono<DokterDto> findByUsername(String username) {
        if (isDokterDoesNotExist(username)){
            throw new DokterDoesNotExistException(username);
        }
        return dokterRepository.findByUsername(username);
    }

    @Transactional
    @Override
    public Mono<DokterDto> uploadCertificate(String username, MultipartFile certificate) throws IOException {
        var filename = username + "-certificate-" + certificate.getOriginalFilename();
        var tempFile = File.createTempFile("caper-", filename);
        certificate.transferTo(tempFile);

        var params = ObjectUtils.asMap(
                "public_id", "caper/" + username + "-certificate" + certificate.getOriginalFilename(),
                "overwrite", true,
                "resource_type", "image"
        );
        var uploadResult = cloudinary.uploader().upload(tempFile, params);
        var url = uploadResult.get("secure_url").toString();
        return dokterRepository.uploadCertificate(username, url);
    }

    public Mono<DokterDto> update(String username, UpdateDokterDto request) {
        if (isDokterDoesNotExist(username)){
            throw new DokterDoesNotExistException(username);
        }
        return dokterRepository.updateDokter(username, request);
    }

    private boolean isDokterDoesNotExist(String username) {
        return dokterRepository.findByUsername(username) == null;
    }

}
