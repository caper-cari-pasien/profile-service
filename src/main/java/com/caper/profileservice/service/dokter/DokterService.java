package com.caper.profileservice.service.dokter;

import com.caper.profileservice.dto.DokterDto;
import com.caper.profileservice.dto.UpdateDokterDto;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import reactor.core.publisher.Mono;

import java.io.IOException;
import java.util.List;

@Service
public interface DokterService {
    Mono<List<DokterDto>> findAll();
    Mono<DokterDto> findByUsername(String username);

    Mono<DokterDto> uploadCertificate(String username, MultipartFile certificate) throws IOException;
    Mono<DokterDto> update(String username, UpdateDokterDto request);
}
