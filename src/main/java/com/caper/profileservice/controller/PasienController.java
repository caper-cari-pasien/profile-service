package com.caper.profileservice.controller;

import com.caper.profileservice.dto.PasienDto;
import com.caper.profileservice.dto.UpdatePasienDto;
import com.caper.profileservice.service.pasien.PasienService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import reactor.core.publisher.Mono;

import java.io.IOException;

@RestController
@RequestMapping("/api/pasien")
@RequiredArgsConstructor
public class PasienController {
    private final PasienService pasienService;
    @GetMapping("/{username}")
    public ResponseEntity<Mono<PasienDto>> getPasienByUsername(@PathVariable String username) {
        Mono<PasienDto> response = pasienService.findByUsername(username);
        return ResponseEntity.ok(response);
    }

    @PutMapping("/update/{username}")
    public ResponseEntity<Mono<PasienDto>> putDokter(@PathVariable String username, @RequestBody UpdatePasienDto request) {
        Mono<PasienDto> response = pasienService.update(username, request);
        return ResponseEntity.ok(response);
    }

    @PatchMapping(value = "/{username}/image", consumes = {MediaType.MULTIPART_FORM_DATA_VALUE})
    public ResponseEntity<Mono<PasienDto>> uploadCertificate(@PathVariable String username,
                                                             @RequestPart("file") MultipartFile certificate) throws IOException {
        Mono<PasienDto> response = pasienService.uploadTeethPicture(username, certificate);
        return ResponseEntity.ok(response);
    }
}

