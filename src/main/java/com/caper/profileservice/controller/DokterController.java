package com.caper.profileservice.controller;

import com.caper.profileservice.dto.UpdateDokterDto;
import com.caper.profileservice.service.dokter.DokterService;
import com.caper.profileservice.dto.DokterDto;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import reactor.core.publisher.Mono;

import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("/api/dokter")
@RequiredArgsConstructor
public class DokterController {
    private final DokterService dokterService;

    @GetMapping("/all")
    public ResponseEntity<Mono<List<DokterDto>>> getAllDokter() {
        Mono<List<DokterDto>> response = dokterService.findAll();
        return ResponseEntity.ok(response);
    }

    @GetMapping("/{username}")
    public ResponseEntity<Mono<DokterDto>> getDokterByUsername(@PathVariable String username) {
        Mono<DokterDto> response = dokterService.findByUsername(username);
        return ResponseEntity.ok(response);
    }

    @PatchMapping(value = "/{username}/image", consumes = {MediaType.MULTIPART_FORM_DATA_VALUE})
    public ResponseEntity<Mono<DokterDto>> uploadCertificate(@PathVariable String username,
                                                             @RequestPart("file") MultipartFile certificate) throws IOException {
        Mono<DokterDto> response = dokterService.uploadCertificate(username, certificate);
        return ResponseEntity.ok(response);
    }

    @PutMapping("/update/{username}")
    public ResponseEntity<Mono<DokterDto>> putDokter(@PathVariable String username, @RequestBody UpdateDokterDto request) {
        Mono<DokterDto> response = dokterService.update(username, request);
        return ResponseEntity.ok(response);
    }
}
