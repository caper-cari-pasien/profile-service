package com.caper.profileservice.exceptions;

public class DokterDoesNotExistException extends RuntimeException {
        public DokterDoesNotExistException(String username){
            super("Dokter with username "+ username + " does not exist");
        }

}
