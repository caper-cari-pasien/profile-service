package com.caper.profileservice.exceptions;

public class PasienDoesNotExistException extends RuntimeException{
    public PasienDoesNotExistException(String username){
        super("Pasien with username "+ username + " does not exist");
    }
}
