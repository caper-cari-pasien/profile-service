package com.caper.profileservice.dto;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class TreatmentDto {
    private String id;
    private String note;
    private Boolean isVerified;
    private Date startTime;
    private OpenTreatmentDto openTreatment;
    private PasienDto patient;
    private TreatmentChatRoomDto chatRoom;
}
