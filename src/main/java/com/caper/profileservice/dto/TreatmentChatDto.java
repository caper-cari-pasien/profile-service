package com.caper.profileservice.dto;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class TreatmentChatDto {
    private String id;
    private String content;
    private Date time;
    private TreatmentChatRoomDto chatRoom;
    private UserDto user;

}
