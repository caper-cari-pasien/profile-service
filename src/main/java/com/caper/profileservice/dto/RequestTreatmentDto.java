package com.caper.profileservice.dto;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class RequestTreatmentDto {
    private String id;
    private String location;
    private Date treatmentTime;
    private String description;
    private PasienDto patient;
}
