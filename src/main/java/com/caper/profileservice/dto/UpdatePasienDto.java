package com.caper.profileservice.dto;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class UpdatePasienDto {
    private UpdateUserDto user;
    private String teethPictureUrl;
}
