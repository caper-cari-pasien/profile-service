package com.caper.profileservice.dto;

public enum UserRoleDto {
    PATIENT, DOCTOR
}
