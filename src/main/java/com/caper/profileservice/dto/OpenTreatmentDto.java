package com.caper.profileservice.dto;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class OpenTreatmentDto {
    private String id;
    private String location;
    private Date treatmentTime;
    private String description;
    private Long price;
    private TreatmentCategoryDto category;
    private List<TreatmentDto> treatmentList;
}
