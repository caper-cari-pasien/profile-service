package com.caper.profileservice.dto;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class TreatmentChatRoomDto {
    private String id;
    private TreatmentDto treatmentDto;
    private List<TreatmentChatDto> chatList;

}
