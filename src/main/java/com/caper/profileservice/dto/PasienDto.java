package com.caper.profileservice.dto;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class PasienDto {
    private String id;
    private UserDto user;
    private String teethPictureUrl;
    private List<RequestTreatmentDto> requestTreatmentList;
    private List<TreatmentDto> treatmentList;
}
