package com.caper.profileservice.repository;

import com.caper.profileservice.dto.PasienDto;
import com.caper.profileservice.dto.UpdatePasienDto;
import com.caper.profileservice.dto.UploadImageDto;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Repository;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.reactive.function.client.WebClientResponseException;
import reactor.core.publisher.Mono;


@Repository
public class PasienRepository {
    private final WebClient client;

    public PasienRepository(@Value("${db-service-url}") String baseUrl) {
        client = WebClient.create(baseUrl + "/api/user/profile/pasien");
    }

    public Mono<PasienDto> findByUsername(String username) {
        try {
            return client.get().uri("/" + username).retrieve().bodyToMono(PasienDto.class);
        } catch (WebClientResponseException e) {
            if (e.getStatusCode().equals(HttpStatus.NOT_FOUND)) {
                return null;
            }
            throw e;
        }
    }

    public Mono<PasienDto> updatePasien(String username, UpdatePasienDto pasien) {
        try{
            return client.put().uri("/update/" + username).body(Mono.just(pasien), UpdatePasienDto.class)
                    .retrieve().bodyToMono(PasienDto.class);
        }catch (WebClientResponseException e) {
            if(e.getStatusCode().equals(HttpStatus.NOT_FOUND)) {
                return null;
            }
            throw e;
        }
    }

    public Mono<PasienDto> uploadTeethPicture(String username, String url) {
        return client.patch().uri("/teeth-picture/" + username)
                .body(Mono.just(UploadImageDto.builder().url(url).build()), UploadImageDto.class)
                .retrieve().bodyToMono(PasienDto.class);
    }
}
