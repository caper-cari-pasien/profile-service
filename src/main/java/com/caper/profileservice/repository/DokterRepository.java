package com.caper.profileservice.repository;

import com.caper.profileservice.dto.DokterDto;
import com.caper.profileservice.dto.UpdateDokterDto;
import com.caper.profileservice.dto.UploadImageDto;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Repository;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.reactive.function.client.WebClientResponseException;
import reactor.core.publisher.Mono;

import java.util.List;


@Repository
public class DokterRepository {
    private final WebClient client;

    public DokterRepository(@Value("${db-service-url}") String baseUrl) {
        client = WebClient.create(baseUrl + "/api/user/profile/dokter");
    }

    public Mono<List<DokterDto>> findAll() {
        try {
            return client.get().uri("/all").retrieve().bodyToFlux(DokterDto.class).collectList();
        } catch (WebClientResponseException e){
            if (e.getStatusCode().equals(HttpStatus.NOT_FOUND)) {
                return null;
            }
            throw e;
        }
    }

    public Mono<DokterDto> findByUsername(String username) {
        try {
            return client.get().uri("/" + username).retrieve().bodyToMono(DokterDto.class);
        } catch (WebClientResponseException e) {
            if (e.getStatusCode().equals(HttpStatus.NOT_FOUND)) {
                return null;
            }
            throw e;
        }
    }

    public Mono<DokterDto> updateDokter(String username, UpdateDokterDto dokter) {
        try{
            return client.put().uri("/update/" + username).body(Mono.just(dokter), UpdateDokterDto.class)
                    .retrieve().bodyToMono(DokterDto.class);
        }catch (WebClientResponseException e) {
            if(e.getStatusCode().equals(HttpStatus.NOT_FOUND)) {
                return null;
            }
            throw e;
        }
    }

    public Mono<DokterDto> uploadCertificate(String username, String url) {
        return client.patch().uri("/certificate/" + username)
                .body(Mono.just(UploadImageDto.builder().url(url).build()), UploadImageDto.class)
                .retrieve().bodyToMono(DokterDto.class);
    }
}
